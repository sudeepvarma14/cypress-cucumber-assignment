Feature: Quotation
    @regression @quotation @request
    Scenario: Request Quotation
        Given When the user is on header page
        And user clicks on request quotation button
        When user provides required data and clicks on save quotation
        Then user should see identification number

    @regression @quotation @retrieve
    Scenario: Retreive Quotation
        Given When the user is on header page
        And user clicks on retreive quotation button
        When user enters the identification number and click on retreive button
        Then quotation should be displayed
