Feature: logout
    @regression @logout
    Scenario: logout
        Given When the user is on header page
        When user clicks on logout button
        Then user should be logged out
