Feature: login page
    @regression @login @sunnyDay
    Scenario: Successful Login
        Given When the user is on login page
        When user enters email "test@test.com" and password "test123" and clicks on Log In button
        Then user should be logged in successfully
    @regression @login @rainyDay
    Scenario: Invalid Login
        Given When the user is on login page
        When user enters email "test11@test.com" and password "test11123" and clicks on Log In button
        Then error message should be displayed
