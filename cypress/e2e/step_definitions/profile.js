import { Given, When, Then } from '@badeball/cypress-cucumber-preprocessor';
import { loginPage } from '@pages/LoginPage';
import { headerPage } from '@pages/HeaderPage';
import { newQuotationPage } from '@pages/NewQuotationPage';
import { retrieveQuotationPage } from '@pages/RetrieveQuotationPage';

Given('user clicks on edit profile button', () => {
	headerPage.clickEditProfileLink();
	headerPage.validateEditProfileTitle();
});
