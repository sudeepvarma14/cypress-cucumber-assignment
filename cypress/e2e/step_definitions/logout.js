import { Given, When, Then } from '@badeball/cypress-cucumber-preprocessor';
import { loginPage } from '@pages/LoginPage';
import { headerPage } from '@pages/HeaderPage';

Given('When the user is on header page', () => {
	cy.visit('/insurance/v1/index.php');
	loginPage.submitLogin('test@test.com', 'test123');
	cy.url().should('contains', '/header.php');
	headerPage.validateEmail('test@test.com');
});

When('user clicks on logout button', () => {
	headerPage.clickLogout();
});

// When('A user provides incorrect credentials, and clicks on the login button', (table) => {
// 	table.hashes().forEach((row) => {
// 		cy.log(row.username);
// 		cy.log(row.password);
// 		loginPage.submitLogin(row.username, row.password);
// 	});
// });
Then('user should be logged out', () => {
	cy.url().should('contains', '/index.php');
});
