import { Given, When, Then } from '@badeball/cypress-cucumber-preprocessor';
import { loginPage } from '@pages/LoginPage';
import { headerPage } from '@pages/HeaderPage';

Given('When the user is on login page', () => {
	cy.visit('/insurance/v1/index.php');
});

When('user enters email {string} and password {string} and clicks on Log In button', (email, password) => {
	loginPage.submitLogin(email, password);
});

// When('A user provides incorrect credentials, and clicks on the login button', (table) => {
// 	table.hashes().forEach((row) => {
// 		cy.log(row.username);
// 		cy.log(row.password);
// 		loginPage.submitLogin(row.username, row.password);
// 	});
// });
Then('user should be logged in successfully', () => {
	cy.url().should('contains', '/header.php');
	headerPage.validateEmail('test@test.com');
	headerPage.validatePageHeader();
});

Then('error message should be displayed', () => {
	loginPage.validateErrorMessage();
});
