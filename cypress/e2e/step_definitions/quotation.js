import { Given, When, Then } from '@badeball/cypress-cucumber-preprocessor';
import { loginPage } from '@pages/LoginPage';
import { headerPage } from '@pages/HeaderPage';
import { newQuotationPage } from '@pages/NewQuotationPage';
import { retrieveQuotationPage } from '@pages/RetrieveQuotationPage';

Given('user clicks on request quotation button', () => {
	headerPage.clickRequestQuotation();
});

Given('user clicks on retreive quotation button', () => {
	headerPage.clickRetrieveQuotation();
});

When('user enters the identification number and click on retreive button', () => {
	cy.fixture('example').then((data) => {
		headerPage.enterItentificationNumber(data.id);
	});
	headerPage.clickGetQuoteButton();
});

When('user provides required data and clicks on save quotation', () => {
	headerPage.selectBreakDownCover('European');
	headerPage.selectWindScreenRepairTrue();
	headerPage.enterIncidents('1');
	headerPage.enterRegistration('123456');
	headerPage.enterAnnMileage('10000');
	headerPage.enterEstimatedValue('65314');
	headerPage.selectParkingLocation('Locked Garage');
	headerPage.selectYear('2015');
	headerPage.selectMonth('January');
	headerPage.selectDay('10');
	headerPage.saveQuotation();
});

Then('user should see identification number', () => {
	newQuotationPage.getIdNumber();
});

Then('quotation should be displayed', () => {
	retrieveQuotationPage.validateTitle();
});
