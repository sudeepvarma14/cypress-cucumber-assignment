class HeaderPage {
	elements = {
		userEmail: () => cy.get('h4'),
		logoutButton: () => cy.contains('Log out'),
		requestQuotationLink: () => cy.get('#ui-id-2'),
		retrieveQuotationLink: () => cy.get('#ui-id-3'),
		profileLink: () => cy.get('#ui-id-4'),
		editProfileLink: () => cy.get('#ui-id-5'),
		brokerInsuranceTitle: () => cy.get('#tabs-1'),
		breakDownCover: () => cy.get('#quotation_breakdowncover'),
		windScreenRepair: () => cy.get('#quotation_windscreenrepair_t'),
		incidents: () => cy.get('#quotation_incidents'),
		registration: () => cy.get('#quotation_vehicle_attributes_registration'),
		annualMileage: () => cy.get('#quotation_vehicle_attributes_mileage'),
		estimatedValue: () => cy.get('#quotation_vehicle_attributes_value'),
		parkingLocation: () => cy.get('#quotation_vehicle_attributes_parkinglocation'),
		stateOfPolicyYear: () => cy.get('#quotation_vehicle_attributes_policystart_1i'),
		stateOfPolicyMonth: () => cy.get('#quotation_vehicle_attributes_policystart_2i'),
		stateOfPolicyDay: () => cy.get('#quotation_vehicle_attributes_policystart_3i'),
		save: () => cy.get('#new_quotation > .actions > .btn-success'),
		indetificationNumberField: () => cy.get('input[name=id]'),
		retrieveButton: () => cy.get('#getquote'),
		editProfileTitle: () => cy.get('h1')
	};

	validateEmail(email) {
		this.elements.userEmail().should('have.text', email);
	}

	validatePageHeader() {
		this.elements.brokerInsuranceTitle().should('contain.text', 'Broker Insurance WebPage');
	}

	clickLogout() {
		this.elements.logoutButton().click();
	}

	clickRequestQuotation() {
		this.elements.requestQuotationLink().click();
	}

	clickRetrieveQuotation() {
		this.elements.retrieveQuotationLink().click();
	}

	selectBreakDownCover(cover) {
		this.elements.breakDownCover().select(cover);
	}

	selectWindScreenRepairTrue() {
		this.elements.windScreenRepair().check({ force: true });
	}

	enterIncidents(count) {
		this.elements.incidents().type(count);
	}

	enterRegistration(reg) {
		this.elements.registration().type(reg);
	}

	enterAnnMileage(val) {
		this.elements.annualMileage().type(val);
	}

	enterEstimatedValue(val) {
		this.elements.estimatedValue().type(val);
	}

	selectParkingLocation(loc) {
		this.elements.parkingLocation().select(loc);
	}

	selectYear(val) {
		this.elements.stateOfPolicyYear().select(val);
	}

	selectMonth(val) {
		this.elements.stateOfPolicyMonth().select(val);
	}

	selectDay(val) {
		this.elements.stateOfPolicyDay().select(val);
	}
	saveQuotation() {
		this.elements.save().click();
	}

	enterItentificationNumber(id) {
		this.elements.indetificationNumberField().type(id);
	}

	clickGetQuoteButton() {
		this.elements.retrieveButton().click();
	}

	clickEditProfileLink() {
		this.elements.editProfileLink().click();
	}

	validateEditProfileTitle() {
		this.elements.editProfileTitle().should('have.text', 'Editing user profile');
	}
}
export const headerPage = new HeaderPage();
