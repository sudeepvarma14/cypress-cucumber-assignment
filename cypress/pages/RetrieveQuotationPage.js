class RetrieveQuotationPage {
	elements = {
		title: () => cy.get('font')

		// errorMessage: () => cy.get('h3[data-test="error"]')
	};

	validateTitle() {
		this.elements.title().should('have.text', 'Retrieve Quotation');
	}
}

export const retrieveQuotationPage = new RetrieveQuotationPage();
