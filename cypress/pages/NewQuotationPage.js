class NewQuotationPage {
	elements = {
		idNumber: () => cy.get('body')
		// errorMessage: () => cy.get('h3[data-test="error"]')
	};

	getIdNumber() {
		this.elements.idNumber().then(function ($ele) {
			const text = $ele.text();
			cy.log(text);
			const arr = text.split(' ');
			const id = arr[10].split('P');
			cy.log(id[0]);
			cy.fixture('example').then((data) => {
				cy.log(data.id);
				data.id = id[0];
				cy.writeFile('cypress/fixtures/example.json', { id: id[0] });
			});
		});
	}
}

export const newQuotationPage = new NewQuotationPage();
